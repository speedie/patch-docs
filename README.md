# patch-docs

suckless patch documentation by me and how some patches could be improved

## autostart

Combined with restartsig, make sure it isn't autostarted if we're restarting. I solved this problem in my [personal build](https://codeberg.org/speedie/speedwm) by writing a temporary file and then checking if it exists when autostarting.

## cool autostart

Combined with restartsig, make sure it isn't autostarted if we're restarting. I solved this problem in my [personal build](https://codeberg.org/speedie/speedwm) by writing a temporary file and then checking if it exists when autostarting.

## awesomebar

In the togglewin() function, add `if (!c) return;` below `Client *c = (Client*)arg->v;`. This prevents a crash which can happen if there are no clients and the bar is clicked.

I have made a patch which does this, see my [patches repository](https://codeberg.org/speedie/patches).

## windowmap

In the window_map and window_unmap functions, at the bottom add `focus(NULL)`. This is because otherwise, if a client isn't focused through other means (such as a cursor hovering over it) no clients will be focused at all. You can see this for yourself by clicking on a tag with a client on it.

NOTE: Incompatible with awesomebar, see [this commit](https://codeberg.org/speedie/speedwm/commit/481e9ca8f1b90b6e6e583777a600773b2d02d884) for how I "fixed" it.

## statuscmd

Write $BUTTON to /tmp/dwm-button instead of using setenv(). This is more of an opinion thing. While it does break compatibility (which is why I kept the old setenv in my own build too) it seems to work much better, despite creating an extra temporary file.

I've done this too, see my [patches repository](https://codeberg.org/speedie/patches).
